from flask import Flask, request, jsonify
from json import JSONEncoder

import json

from keras.models import load_model
from keras.preprocessing.image import load_img, img_to_array, array_to_img
from PIL import Image
import numpy as np

app = Flask(__name__)

global model
global default_dims 

#img dims for model
default_dims = (384, 384)

#Load Model
model = load_model('cityscape_seg_FPN.h5')

#Combine all the masks together
def c_mask(masks, dims=default_dims):
    output = np.empty(dims + (1,))
    
    for x in range(0, masks.shape[0]):
        for y in range(0, masks.shape[1]):
                   target = masks[x][y]
                   output[x][y] = np.argmax(target) 
    return output

class Numpy_Array_Encoder(JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.ndarray):
            return obj.tolist()

        return JSONEncoder.default(self, obj)

@app.route("/prediction", methods=["POST"])
def process_image():
    file = request.files['image']
    
    img = Image.open(file.stream)
    resize_img = img.resize(default_dims)

    img_model = np.empty((1, (*default_dims + (3,))), dtype='float')

    img_model[0,] = img_to_array(resize_img)#Normalisation

    predict = model.predict(img_model / 255.)
    predict_2d = c_mask(predict[0])
    result = json.dumps(predict_2d, cls=Numpy_Array_Encoder)

    return jsonify({'msg': 'success', 'prediction': result })


if __name__ == "__main__":
    app.run(debug=True)
